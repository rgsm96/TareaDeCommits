
function clearUserInput() {
    const input =document.querySelector("#letterTextBox")
    input.value="";
}

function getEnteredLetter() {
    const letter = document.querySelector("#letterTextBox");
    return letter.value;
}

function updateGallows(stage) {
    const gallow=document.querySelector("#gallows img");
    gallow.src="img/"+stage+".png";
}

function updateRevealed(text) {
   const upRevealed =document.querySelector("#revealed");
   upRevealed.innerHTML=text;
}

function showWinnerPane() {
    const pane=document.querySelector("#winnerPane");
    pane.classList.remove("hidden");
}

function showLoserPane() {
    const pane=document.querySelector("#loserPane");
    pane.classList.remove("hidden");
}

function showPlayAgainPane() {
    const pane=document.querySelector("#playAgainPane");
    pane.classList.remove("hidden");
}

function hideUserInput() {
    const pane=document.querySelector("#userInput");
    pane.classList.add("hidden");
}

function updateControls(hasWon, hasLost) {
    if(hasWon||hasLost){
        if(hasWon){
            showWinnerPane();
        }else if(hasLost){
            showLoserPane();
        }
    clearUserInput();
    showPlayAgainPane();
    }
    else {
        clearUserInput();
    }
}


function onClick() {
    processUserInput();
}

function onKeyUp(event) {
    if (event.keyCode === 13) {
        onClick();
    }
}

function setupEventHandlers() {
    const but=document.querySelector("button");
    but.addEventListener("click", onClick)

    /* TODO Agregar la funcion onKeyUp como eventHandler del evento keyup en el textbox */
    const tbox=document.querySelector("#letterTextBox");
    tbox.addEventListener("keyup", onKeyUp);
}